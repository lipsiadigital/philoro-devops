#!/usr/bin/env bash

version=$1
path="$PWD/lipsia/production"
services=('portal-frontend' 'portal-backend')

if [ -z $version ]; then
  echo "Provide a version"
  exit 1
fi

for service in "${services[@]}"; do
  servicePath=$path/$service
  bash "$PWD/scripts/functions/bumpVersion.sh" "$servicePath" "$version"
  bash "$PWD/scripts/functions/restart.sh" "$servicePath" "$service"
done
