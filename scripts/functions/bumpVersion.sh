#!/usr/bin/env bash

echo -e "Updating file $1/deployment.yaml to version $2"
sed -i -r "s/\(image:.*:\).*/\1$2/g" $1/deployment.yaml
