#!/usr/bin/env bash

echo "Restarting service $2 with path $1/deployment.yaml"

kubectl delete deployment $2

if [[ "$2" == *"backend"* ]]; then
  bash "$PWD/scripts/functions/resetDb.sh" "$1" "$2"
fi

kubectl apply -f $1/deployment.yaml
