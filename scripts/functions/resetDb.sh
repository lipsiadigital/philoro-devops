#!/usr/bin/env bash

echo "Resetting database of service $2 with path $1/deployment.yaml"

host=$(cat $1/env-secret.yaml | grep PG_HOST | grep -oP '(?<=").*(?=")')
user=$(cat $1/env-secret.yaml | grep PG_USER | grep -oP '(?<=").*(?=")')
dbname=$(cat $1/env-secret.yaml | grep PG_DB | grep -oP '(?<=").*(?=")')

export "PGPASSFILE=$PWD/.pgpass"
echo "psql -h $host -U $user -q $dbname"
psql -h $host -U $user -t -c "SELECT 'DROP TABLE IF EXISTS ' || tablename || ';' AS columns FROM pg_tables WHERE schemaname = 'public'" $dbname | psql -h $host -U $user -q $dbname
