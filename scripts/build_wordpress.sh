#! /usr/bin/env bash

src_repo=docker.io/library
src_name=wordpress
src_tag=6.1.1-php8.0-apache
dst_repo=philoro-registry.01.pscluster.net/philoro

src_image="$src_repo/$src_name:$src_tag"
dst_image="$dst_repo/$src_name:$src_tag"

docker pull "$src_image"
docker tag "$src_image" "$dst_image"
docker push "$dst_image"
