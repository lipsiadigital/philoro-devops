List resources to be used with get and describe: `kubectl api-resources`

Get pod name: `kubectl get pods`
Open shell in container: `kubectl exec portal-backend-564b8f97dd-znhqv -it bash`
Get pod logs: `kubectl logs portal-backend-564b8f97dd-znhqv`
See events (useful if there's a problem with the yamls): `kubectl get events`

Create a basic auth secret to use in ingress labels:

```
htpasswd -c auth lipsia
kubectl create secret generic prod-basic-auth --from-file=auth=auth
```

Docker login in the cluster:

```
kubectl create secret generic regcred --from-file=.dockerconfigjson=dockerconfig.json --type=kubernetes.io/dockerconfigjson
```

kubectl get ingress -o yaml monitoring-internex > ingress-monitoring-internex.yaml
kubectl get deployment -o yaml monitoring-internex > deployment-monitoring-internex.yaml
kubectl get svc -o yaml monitoring-internex > service-monitoring-internex.yaml

kubectl create -f lipsia/deployment-whoami.yaml
kubectl logs deployment/whoami
kubectl get events
kubectl delete deployment whoami

docker login philoro-registry.01.pscluster.net
docker tag docker.io/containous/whoami:v1.5.0 philoro-registry.01.pscluster.net/philoro/whoami:v1.5.0
docker push philoro-registry.01.pscluster.net/philoro/whoami:v1.5.0

kubectl create -f lipsia/staging-issuer.yaml
kubectl describe issuer letsencrypt-staging

kubectl replace -f lipsia/ingress-whoami.yaml
kubectl describe certificate whoami-tls

## Cluster Ingress IPs

144.208.9.118 (Produktion)
144.208.9.119 (Staging)

## Wildcard Domains

_.118.pscluster.net (Produktion)
_.119.pscluster.net (Staging)

## Run

1. Install kubectl
2. Copy kubeconfig to `~/.kube/config`
3. Insert token from bitwarden
4. Set the context (philoro or philoro-prod) `kubectl config use-context philoro`
5. Login to philoro docker registry `docker login philoro-registry.01.pscluster.net` (see bitwarden)

## Update backend

1. Update image in repository (bump image version and run build script)
2. Update image in `lipsia/portal-backend/deployment`
3. Run `kubectl apply -f lipsia/portal-backend/deployment.yaml`
4. Check if deployment was successful `kubectl logs deployment/portal-backend` (metadata.name) and `kubectl describe deployment portal-backend` and `kubectl get pods` should show 1/1
5. If the deployment failed, delete it: `kubectl delete deployment portal-backend` (or re-run deployment with previous image version)

## Env secret updates

```
kubectl replace -f lipsia/staging/portal-backend/env-secret.yaml
kubectl rollout restart deployment portal-backend
```

## TODO Prod deployment

1. Copy resource yamls for staging, update NS
2. Docker login, create issuers (test with LE staging issuer first)
3. Update Ingress with Prod-Domains and redirects
4. Deploy in this order: secret (env), deployment, service, ingress
5. Update ingress with LE prod issuer

## WP Updates

```
tar zvcf content.tar.gz ./assets/wp-content/plugins ./assets/wp-content/themes ./assets/wp-content/uploads
kubectl get pods
kubectl cp content.tar.gz web-nginx-865755cd5f-rpfxx:/tmp/

kubectl exec -it web-nginx-865755cd5f-rpfxx -- bash
cd /tmp
tar zxf content.tar.gz
cp -r assets/wp-content/* /var/www/html/wp-content/

# convert dump with script from wp-prod (philoro branch)
# load dump using dbeaver (make sure you have the correct db user!)

grep -rl backend.119.pscluster.net /var/www/html/wp-content | xargs sed -i 's/backend.119.pscluster.net/backend.edelmetallsparer.com/g'
grep -rl wpdb /var/www/html/wp-content/plugins | xargs sed -E -i 's/(\s)wpdb(\s|$)/\1\\wppg_db\2/g'
wp user update philoro-admin --user_pass=m4tchw3rk1337
wp option update home 'https://www.edelmetallsparer.com'
wp option update siteurl 'https://www.edelmetallsparer.com'
wp search-replace 'https://philoro-dev.elje.dev' 'https://www.edelmetallsparer.com' --precise --skip-columns=guid
```

## Prod Domains

### Hauptdomain

www.edelmetallsparer.com
portal.edelmetallsparer.com
backend.edelmetallsparer.com

### Redirects

edelmetallsparer.com -> www.edelmetallsparer.com
edelmetallsparen.com -> www.edelmetallsparer.com
www.edelmetallsparen.com -> www.edelmetallsparer.com
edelmetallsparer.at -> www.edelmetallsparer.com?locale=at
www.edelmetallsparer.at -> www.edelmetallsparer.com?locale=at
edelmetallsparen.at -> www.edelmetallsparer.com?locale=at
www.edelmetallsparen.at -> www.edelmetallsparer.com?locale=at
edelmetallsparer.ch -> www.edelmetallsparer.com?locale=ch
www.edelmetallsparer.ch -> www.edelmetallsparer.com?locale=ch
edelmetallsparen.ch -> www.edelmetallsparer.com?locale=ch
www.edelmetallsparen.ch -> www.edelmetallsparer.com?locale=ch
edelmetallsparer.de -> www.edelmetallsparer.com?locale=de
www.edelmetallsparer.de -> www.edelmetallsparer.com?locale=de

### ELJE Deployment

#### First Time

1. Install kubectl
2. Copy kubeconfig to `~/.kube/config`
3. Insert password from chat
4. Set the context (philoro or philoro-prod) `kubectl config use-context philoro`
5. Login to philoro docker registry `docker login philoro-registry.01.pscluster.net` (see bitwarden)

#### Dpeloyment Tasks

Set the correct context for your deployment

`kubectl config use-context philoro` for staging
`kubectl config use-context philoro-prod` for prod

```
tar zvcf content.tar.gz ./assets/wp-content/plugins ./assets/wp-content/themes ./assets/wp-content/uploads
kubectl get pods
kubectl cp content.tar.gz web-nginx-865755cd5f-rpfxx:/tmp/

kubectl exec -it web-nginx-865755cd5f-rpfxx -- bash
cd /tmp
tar zxf content.tar.gz
cp -r assets/wp-content/* /var/www/html/wp-content/
```

## Betriebsgold

portal.betriebsgold.de => 144.208.9.118
portal.betriebs-gold.de => portal.betriebsgold.de
backend.portal.betriebs-gold.de => 144.208.9.118

## How to run deployment scripts

You need the postgres client `psql` installed.

1. Decide where to deploy. For example on all staging emsp / ema services.
2. Run the corresponding script from root `./scripts/staging/ema_emsp.sh`
3. Profit
